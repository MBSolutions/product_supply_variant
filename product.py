# -*- coding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Bool, Eval
from trytond.pool import PoolMeta, Pool
from trytond import backend


__all__ = ['Template', 'Product', 'ProductSupplier']


class Template:
    __metaclass__ = PoolMeta
    __name__ = 'product.template'

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls.product_suppliers = fields.Function(fields.One2Many(
                'purchase.product_supplier', 'product', 'Suppliers'),
                'get_product_supplier'
                )

    @classmethod
    def view_attributes(cls):
        return super(Template, cls).view_attributes() + [
            ('//page[@id="suppliers"]', 'states', {
                    'invisible': Bool(True),
                    }),
            ]

    def get_product_supplier(self, name):
        """Reached here! Just raise an exception to know what logic is using
        product template's product_supplier.
        NB:
        To not trigger the field from the template, it must be replaced in xml!
        """
        raise Exception(
            'Product Suppliers must be taken from product.product aka variant.'
        )


class Product:
    __metaclass__ = PoolMeta
    __name__ = 'product.product'

    product_suppliers = fields.One2Many('purchase.product_supplier',
        'product', 'Suppliers', states={
            'readonly': ~Eval('active', True),
            'invisible': (~Eval('purchasable', False)
                | ~Eval('context', {}).get('company')),
            }, depends=['active', 'purchasable'])
    # The following fields are re-defined here to be editable on the variant
    purchase_uom = fields.Function(fields.Many2One('product.uom',
            'Purchase UOM', states={
                'readonly': ~Eval('active'),
                'invisible': ~Eval('purchasable'),
                'required': Eval('purchasable', False),
                },
            domain=[('category', '=', Eval('default_uom_category'))],
            depends=['active', 'purchasable', 'default_uom_category'],
            help='Note: This field is displayed for convenience on the '
            'variant, but refers to the template.',
            ),
        'get_template', setter='set_template', searcher='search_template')
    supply_on_sale = fields.Function(fields.Boolean('Supply On Sale',
            states={
                'invisible': ~Eval('purchasable') | ~Eval('salable'),
                },
            depends=['purchasable', 'salable'],
            help='Note: This field is displayed for convenience on the '
            'variant, but refers to the template.',
            ),
        'get_template', setter='set_template', searcher='search_template')


class ProductSupplier:
    __metaclass__ = PoolMeta
    __name__ = 'purchase.product_supplier'

    @classmethod
    def __setup__(cls):
        super(ProductSupplier, cls).__setup__()
        cls.product = fields.Many2One('product.product', 'Variant',
            required=True, ondelete='CASCADE', select=True)

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')

        super(ProductSupplier, cls).__register__(module_name)
        # Migration for moving purchase.product_supplier to product.product
        # Foreign key must be updated in code
        table = TableHandler(cls, module_name)
        table.drop_fk('product')
        table.add_fk('product', 'product_product', on_delete='CASCADE')
